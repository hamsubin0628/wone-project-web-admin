import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import testDataConstants from '~/store/modules/test-data/constants'
Vue.prototype.$testDataConstants = testDataConstants

import memberConstants from '~/store/modules/member/constants'
Vue.prototype.$memberConstants = memberConstants

import boardConstants from '~/store/modules/board/constants'
Vue.prototype.$boardConstants = boardConstants

import cardConstants from '~/store/modules/card/constants'
Vue.prototype.$cardConstants = cardConstants

import faqConstants from '~/store/modules/faq/constants'
Vue.prototype.$faqConstants = faqConstants
