const BASE_URL = '/v1/member'

export default {
    DO_USER_LIST: `${BASE_URL}/all`, //get 사용자 리스트 전체 조회
    DO_USER_DETAIL: `${BASE_URL}/detail/admin/{id}`, //get 사용자 상세보기
    DO_LIST_PAGING: `${BASE_URL}/all/{pageNum}`, //get
}
