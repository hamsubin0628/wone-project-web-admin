export default {
    DO_NOTICE_LIST: 'board/doNoticeList', //get 공지사항만 조회
    DO_NOTICE_LIST_PAGING: 'faq/doNoticeListPaging', //get 공지사항 페이징
    DO_GUIDE_LIST: 'board/doGuideList', //get 이용방법만 조회
    DO_GUIDE_LIST_PAGING: 'faq/doGuideListPaging', //get 이용방법 페이징
    DO_BOARD_DELETE: 'board/doBoardDelete', //delete 게시판 삭제
    DO_NOTICE_CREATE: 'board/doNoticeCreate', //post 공지사항 등록
    DO_GUIDE_CREATE: 'board/doGuideCreate', //post 공지사항 등록
    DO_BOARD_DETAIL: 'faq/doBoardDetail', //get 이용방법 상세 조회
    DO_BOARD_UPDATE: 'faq/doBoardUpdate', //put 이용방법 수정
}
