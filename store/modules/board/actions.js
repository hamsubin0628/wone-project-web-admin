import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_NOTICE_LIST]: (store) => {
        return axios.get(apiUrls.DO_NOTICE_LIST)
    },
    [Constants.DO_NOTICE_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_NOTICE_LIST_PAGING.replace('{pageNum}', payload.pageNum) )
    },
    [Constants.DO_GUIDE_LIST]: (store) => {
        return axios.get(apiUrls.DO_GUIDE_LIST)
    },
    [Constants.DO_GUIDE_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_GUIDE_LIST_PAGING.replace('{pageNum}', payload.pageNum) )
    },
    [Constants.DO_BOARD_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_BOARD_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_BOARD_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_BOARD_UPDATE.replace('{id}', payload.id), payload.data)
    },
    [Constants.DO_BOARD_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_BOARD_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_NOTICE_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_NOTICE_CREATE, payload)
    },
    [Constants.DO_GUIDE_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_GUIDE_CREATE, payload)
    },
}
