const BASE_URL = '/v1/board'

export default {
    DO_NOTICE_LIST: `${BASE_URL}/notice/all?boardType=NOTICE`, //get 공지사항만 조회
    DO_NOTICE_LIST_PAGING: `${BASE_URL}/notice/all?boardType=NOTICE`, //get 공지사항 페이징
    DO_GUIDE_LIST: `${BASE_URL}/use/all`, //get 이용방법만 조회
    DO_GUIDE_LIST_PAGING: `${BASE_URL}/notice/all?boardType=GUIDE`, //get 이용방법 페이징
    DO_BOARD_DETAIL: `${BASE_URL}/detail/notice/{id}`, //get 공지사항 상세조회
    DO_BOARD_UPDATE: `${BASE_URL}/change-content/{id}`, //put 게시판 수정
    DO_BOARD_DELETE: `${BASE_URL}/{id}`, //del 게시판 삭제
    DO_NOTICE_CREATE: `${BASE_URL}/new/notice`, //post 공지사항 등록
    DO_GUIDE_CREATE: `${BASE_URL}/new/guide`, //post 공지사항 등록
}
