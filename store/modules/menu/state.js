const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            parentIcon: 'el-icon-s-home',
            menuLabel: [
                { icon:'el-icon-data-analysis', id: 'DASH_BOARD', currentName: 'Dash Board', link: '/', isShow: true },
            ]
        },
        {
            parentName: '서비스 관리',
            parentIcon: 'el-icon-edit-outline',
            menuLabel: [
                { icon:'el-icon-s-tools', id: 'SERVICE_SETTING',  currentName: '서비스 관리', link: '/service/service-setting', isShow: true },
                { icon:'el-icon-user-solid', id: 'USER_LIST',  currentName: '사용자 리스트', link: '/member/user-list', isShow: true },

            ]
        },
        {
            parentName: '게시판',
            parentIcon: 'el-icon-warning',
            menuLabel: [
                { icon:'el-icon-s-comment', id: 'BOARD_ASK',  currentName: '문의사항', link: '/board/faq/board-ask', isShow: true },
                { icon:'el-icon-document', id: 'BOARD_NOTICE',  currentName: '공지사항', link: '/board/notice/board-notice', isShow: true },
                { icon:'el-icon-s-opportunity', id: 'BOARD_GUIDE',  currentName: '이용방법', link: '/board/guide/board-guide', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
